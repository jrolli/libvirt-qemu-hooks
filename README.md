# QEMU Hooks for `libvirt`
A set of helper scripts that hook into `libvirt` to expand its capabilities.

## Mirror / Span Port Support
Scripts that allow custom metadata in the libvirt XML definition of the
VM to trigger automated handling of a mirror off of the OpenVSwitch
controller.

### 10-mirror-setup.py
(`qemu.d/start`) Identifies the target switch, VLANs, and destination port.
### 10-mirror-teardown.py
(`qemu.d/stopped`) Gracefully tearsdown a mirror that is no longer needed.
