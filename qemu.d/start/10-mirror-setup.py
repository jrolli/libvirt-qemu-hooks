#!/usr/bin/env python
#
# XML SCHEMA (namespace = rnix <http://www.rollinix.net/xml/>)
#   <domain>                 # TOPLEVEL for all libvirt domains
#     <metadata>             # Wrapper for 3rd-party metadata
#       <rnix:kvm>           # Wrapper for RolliNix KVM metadata
#         <rnix:mirror_port> # Mirror_port declaration
#                            #   Attributes:
#                            #     name: ovs mirror name
#                            #     bridge: ovs bridge
#                            #     interface: Linux interface for SPAN
#           <rnix:vlan/>     # Source VLANs (w/ tag attribute)
#           <rnix:vlan/>     #   Repeat as necessary
#         </rnix:mirror_port>
#       </rnix:kvm>
#     </metadata>
#   </domain>

from xml.etree import ElementTree

import subprocess
import sys

namespaces = {'rnix': 'http://www.rollinix.net/xml/'}

root = ElementTree.parse(sys.stdin).getroot()
tree = root.find('metadata')

for port in tree.find('rnix:kvm', namespaces).findall('rnix:mirror_port', namespaces):
  bridge = port.attrib['bridge']
  iface  = port.attrib['interface']
  name   = port.attrib['name']
  vlans  = []
  for vlan in port.findall('rnix:vlan', namespaces):
    vlans.append(vlan.attrib['tag'])
  subprocess.call("ovs-vsctl -- --id=@p get Port %s"
                           " -- --id=@m create Mirror name=%s select-vlan=%s output-port=@p select-all=true"
                           " -- add Bridge %s mirrors @m" %
                  (iface, name, ",".join(vlans), bridge), shell=True)
